# Moderate your instance

## Moderators

When you create/update another user, you can put it as "moderator".
A moderator can:
 * Manage videos blacklist
 * Manage video abuses
 * Remove any video, channel, playlist or comment
 * Update any video
 * See all the videos (including unlisted and private videos)
 * Mute accounts/instances for the entire instance
 * Manage regular users (update, ban, delete)
 
## Video abuses

Any local or remote users can report a video with a particular "reason".
All video abuses are listed in `Administration > Moderation > Video abuses`
Admins/moderators can act on the video (updating/removing/blacklisting it for example).

Admins/moderators can:
 * Delete the report
 * Mark it as accepted/rejected (this is just an indicator for other admins/moderators, it will not send a message to the account that sent the report... yet)
 * Update the moderation comment (is only displayed to other admins/moderators)

## Mute accounts/instances

See https://docs.joinpeertube.org/#/use-mute

## Videos blacklist

### Manual blacklist

Admins/moderators can blacklist any videos (local or remote).
When you blacklist a local video that is already federated, you can choose to **Unfederate** it, meaning that the video will be removed from other instances.
Manually blacklisted videos are listed in `Administration > Moderation > Manually blacklisted videos`.

### Auto blacklist

You can enable this feature in `Administration > Configuration > Basic configuration`.
When enabled, every new uploaded videos will be hidden by default until a moderator manually approves them.
Automatically blacklisted videos are listed in `Administration > Moderation > Auto-blacklisted videos`.

You can then individually allow users you trust to bypass videos quarantine in `Administrations > Users > Update a user`.

## Tips

### Dealing with spam

With open registrations on your server, chances are you will have to deal with spam.
There is no simple and definitive way to solve this problem while keeping registrations open,
but there are several that could help you limit spam and its impact on your instance.

Here are some things that could help:
 * **Enable videos quarantine/auto video blacklisting**
 * **Disable new registrations:** you can disable new registrations, and update your instance description to explain that you accept new users on demand. They can for example use the contact button of the about page.
 * **Require email verification:** force email verification on signup in `Administration > Configuration > Basic configuration`. Unfortunately this feature is not really useful since most spammers use a real email account (gmail etc) and automate the validation process.


### Understand what happens on your instance

To check what videos are uploaded on your server, or what your users do you can:
 * Check your [standard & audit logs](https://docs.joinpeertube.org/#/admin-logs)
 * List all private/unlisted/public videos in the `Local` page, and then click on `Display unlisted and private videos`
 * Subscribe to your global videos feed (for example: https://peertube2.cpy.re/feeds/videos.xml?sort=-publishedAt&filter=local)
 * Subscribe to your global comments feed (for example: https://peertube2.cpy.re/feeds/video-comments.xml?sort=-publishedAt&filter=local)
