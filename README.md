# PeerTube documentation

## Install

Run npm install, and copy files in `vendor/`.

```
$ ./install.sh
```

## Run

```
$ docsify serve . 
```
